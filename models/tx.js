'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tx = sequelize.define('Tx', {
    txid: DataTypes.STRING,
    total: DataTypes.INTEGER,
    timestamp: DataTypes.INTEGER,
    blockhash: DataTypes.STRING
  }, {});
  Tx.associate = function(models) {
    // associations can be defined here
  };
  return Tx;
};
