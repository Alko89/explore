'use strict';

const Tx = require('./tx');

module.exports = (sequelize, DataTypes) => {
  const Address = sequelize.define('Address', {
    a_id: DataTypes.STRING,
    balance: DataTypes.INTEGER
  }, {});
  Address.associate = function(models) {
    // associations can be defined here
    Address.hasMany(Tx, {as: "txs"})
  };
  return Address;
};
