var restify = require('restify');
var explorer = require("./explorer");

function getInfo(req, res, next) {
  explorer.getInfo(function(result) {
    res.send(result);
  });

  next();
}

function getBlockHash(req, res, next) {
  explorer.getBlockHash(req.params.height, function(result) {
    res.send(result);
  });

  next();
}

function getBlock(req, res, next) {
  explorer.getBlock(req.params.hash, function(result) {
    res.send(result);
  });

  next();
}

function getBlockByHeight(req, res, next) {
  explorer.getBlockByHeight(req.params.height, function(result) {
    res.send(result);
  });

  next();
}

function getRawTransaction(req, res, next) {
  explorer.getRawTransaction(req.params.txid, function(result) {
    res.send(result);
  });

  next();
}

var server = restify.createServer();
server.get('/', getInfo);
server.get('/getinfo', getInfo);
server.get('/getblockhash/:height', getBlockHash);
server.get('/getblock/:hash', getBlock);
server.get('/getblockbyheight/:height', getBlockByHeight);
server.get('/getrawtransaction/:txid', getRawTransaction);

server.listen(8080, function() {
  console.log('%s listening at %s', server.name, server.url);
});
